const form = document.getElementById('form')

form.addEventListener ('submit', handleSubmission)

function handleSubmission(event) {
    event.preventDefault()

    const english = parseFloat(document.getElementById('englishMarks').value)
    const maths = parseFloat(document.getElementById('mathsMarks').value)
    const physics = parseFloat(document.getElementById('physicsMarks').value)
    const chemistry = parseFloat(document.getElementById('chemistryMarks').value)
    const biology = parseFloat(document.getElementById('biologyMarks').value)

    const totalMarks = english + maths + physics + chemistry + biology
    let outOfMarks = 500
    findPercentage = (totalMarks / outOfMarks) * 100
    let roundPercentage = Math.round(findPercentage)
    
    document.getElementById('marks').innerHTML = totalMarks + " / 500"
    document.getElementById('percentage').innerHTML = roundPercentage + "%"

    if (roundPercentage >= 90) {
        document.getElementById('grade').innerHTML = 'A+'
    }
    else if (roundPercentage >= 80) {
        document.getElementById('grade').innerHTML = 'A'
    }
    else if (roundPercentage >= 70) {
        document.getElementById('grade').innerHTML = 'B+'
    }
    else if (roundPercentage >= 60) {
        document.getElementById('grade').innerHTML = 'B'
    }
    else if (roundPercentage >= 50) {
        document.getElementById('grade').innerHTML = 'C+'
    }
    else if (roundPercentage >= 40) {
        document.getElementById('grade').innerHTML = 'C'
    }
    else {
        document.getElementById('grade').innerHTML = 'Failed'
    }

    form.reset()
}